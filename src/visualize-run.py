#!/usr/bin/env python3
# -*- coding: utf-8 name> -*-

import numpy as np
from mnist import MNIST
import seaborn as sns
import sys
import argparse
import os
from datetime import date
from datetime import datetime
import logging
import json
import random
import matplotlib
from matplotlib import pyplot as plt

plt.style.use('seaborn')

NUM_OUTPUT_UNITS=10

def sigmoid(z): 
    """ 
        The hidden and output units should use the sigmoid activation function. 
    """
    return 1.0/(1+np.exp(-np.array(z)))

def random_matrix(dimensions, min_weight=-0.5, max_weight=0.5):
    """
    Random weights used to initialize the stored weights of a 
    neural network.
    Input:
        - dimensions: dimensions of the random matrix
        - min_weight: 
        - max_weight: 
    """
    random_seed = int('0xdeadbeef', 16)
    rng = np.random.default_rng(random_seed)
    return rng.uniform(low=min_weight, high=max_weight, size=dimensions)

def create_network(num_inputs, num_output_units, num_hidden_layers, num_hidden_units_per_layer):
    """
    Initialize the nueral network as a collection of weights, 
    from inputs to hidden units and from hidden units to output units.
    """
    num_bias_units = 1 
    output_units = random_matrix((num_hidden_units_per_layer + num_bias_units, num_output_units))
    hidden_units = random_matrix((num_inputs + num_bias_units, num_hidden_units_per_layer))
    return (output_units, hidden_units)

def run_forward_phase(scaled_image, nueral_network):
    """ 
        Compute hidden unit and output activations 
        for the neural networ the value of biases 
        activations are hardcoded to the 1 
        for each layer.
    """
    output_units, hidden_units  = nueral_network
    scaled_image_with_bias = np.array(np.append([1], scaled_image))
    pre_activation_result = np.matmul(scaled_image_with_bias, hidden_units)

    hidden_unit_activations = np.array(sigmoid(pre_activation_result))

    hidden_unit_activations_with_bias = np.array(np.append([1], hidden_unit_activations))
    output_unit_activations = sigmoid(np.matmul(hidden_unit_activations_with_bias, output_units))
    return (output_unit_activations, hidden_unit_activations)

def select_output_activations(forward_phase_result) :
    """
    Seletor for getting activations from the last layer, also 
    known as the output units.
    """
    output_unit_activations,_ = forward_phase_result  
    return output_unit_activations

def select_hidden_unit_activations(forward_phase_result):
    """
    Selector for layer for getting activations from hidden unit
    to the output weights.
    """
    _, hidden_unit_activations = forward_phase_result  
    return hidden_unit_activations


def run_backward_phase(nueral_network, 
                        forward_phase_activations,
                        input_activations,       # This will not include the biases, bias activation will always be 1  
                        true_label,
                        prev_output_unit_updates, 
                        prev_hidden_unit_updates,
                        learning_rate,
                        momentum):
   """
    Decription:
        Runs the backwards propogation for the two layer nueral network.
    Parameters:
   """
   # Use the incoming weights of the output unists here:
   #    connections from hidden units to the output units:  h_i ->  
   #    weights from input units to hidden units   
   weights_to_output_unit, weights_to_hidden_units = nueral_network

   output_activations = select_output_activations(forward_phase_activations)
   hidden_unit_activations = select_hidden_unit_activations(forward_phase_activations)

   """ Run backward phase."""
   delta_errors = np.zeros(len(output_activations))

   # 0. Compute output activation deltas
   # There is no bias needed here!
   for idx,activation in zip(range(len(output_activations)), output_activations): 
       true_label_value = 0.9 if idx == true_label else 0.1
       # \delta_k <- \o_k*(1-o_k)*(t_k  - a_k)
       delta_errors[idx] = activation * (1 - activation) * (true_label_value - activation) # 10 activation deltas ? 
      
   hidden_unit_errors = np.zeros(len(hidden_unit_activations))

   for idx, hidden_unit_activation in zip(range(len(hidden_unit_activations)), hidden_unit_activations):
       # $ \delta_j = h_j(1-h_j)(\sum w_{kj} \delta_k $
       weights_to_output_unit_without_bias = weights_to_output_unit[1:]
        # hidden unit activation deltas should NOT include the bias term.
       hidden_unit_errors[idx] = hidden_unit_activation * (1 - hidden_unit_activation) \
                                            * (np.matmul(weights_to_output_unit_without_bias[idx], delta_errors))

   # This will include the bias weights 
   scaled_output_layer_weight_update_without_bias = np.zeros(np.shape(weights_to_output_unit)) 

   # Activation Delta 
   scaled_output_layer_weight_update_without_bias = np.multiply(learning_rate,  np.matmul(np.transpose([delta_errors]), [hidden_unit_activations]))

   # Bias update calculation is different.
   scaled_output_layer_weight_update_for_bias = np.multiply(learning_rate, [delta_errors]) 

   # Multipy input activations wiht the computed 
   scaled_hidden_layer_weight_update_without_bias = np.multiply(learning_rate, np.matmul(np.transpose([input_activations]), [hidden_unit_errors]))
   scaled_hidden_layer_weight_update_for_bias = np.multiply(learning_rate, [hidden_unit_errors]) 

   # Apply the weight updates for the output units.
   scaled_output_layer_weight_update_including_bias = np.insert(scaled_output_layer_weight_update_without_bias, 0, # column 0
                                                                   scaled_output_layer_weight_update_for_bias,     # all scaled values
                                                                   axis=1)

   # Why do I need to transpose it again ? 
   scaled_output_layer_weight_update_including_bias_after_transpose =  np.transpose(scaled_output_layer_weight_update_including_bias)
   scaled_output_layer_weight_update_including_bias_after_transpose = np.add(scaled_output_layer_weight_update_including_bias_after_transpose, 
                                                                           np.multiply(momentum, prev_output_unit_updates))
   
   weights_to_output_unit = np.add(weights_to_output_unit, scaled_output_layer_weight_update_including_bias_after_transpose) 


   # Apply the weight updates for the hidden units, row input
   scaled_hidden_layer_weight_update_including_bias = np.insert(scaled_hidden_layer_weight_update_without_bias, 0, 
                                                                   [scaled_hidden_layer_weight_update_for_bias], 
                                                                   axis=0) 

   scaled_hidden_layer_weight_update_including_bias = np.add(scaled_hidden_layer_weight_update_including_bias, 
                                                               np.multiply(momentum, prev_hidden_unit_updates))

   #compute and add momentum
   weights_to_hidden_units = np.add(weights_to_hidden_units, scaled_hidden_layer_weight_update_including_bias)
   
   
   return (weights_to_output_unit, 
               weights_to_hidden_units, 
               scaled_output_layer_weight_update_including_bias_after_transpose,  
               scaled_hidden_layer_weight_update_including_bias)

def select_random_subset(images, labels, retain_percent=1):
    total = len(labels)
    retained = round(retain_percent*total)
    if retained > total: raise ValueError('Retained traiing set cannot be greater than total');
    elif retained <= 0: raise ValueError('Must retain non-zero number entries');
    return random.sample(list(zip(images, labels)), retained)

"""
Preprocessing: Scale the data values to be between 0 and 1 by dividing by 255.
Initial weights: Your network should start off with small (−.05< w < .05) 
random positive and negative weights.
"""
def train(max_epochs=50, 
            learning_rate = 0.1, 
            momentum = 0.9,
            num_hidden_units_per_layer = 20, 
            percent_training_set = 1,
            name="default"):

    mndata = MNIST('../data')
    training_images, training_labels = mndata.load_training()
    test_images, test_labels = mndata.load_testing()

    num_hidden_layers = 1
    num_output_units = 10
    num_inputs = 784

    print("START_TRAINING", 
                "NUM_INTPUTS", num_inputs, 
                "NUM_OUTPUT_UNITS", num_output_units,
                "MOMENTUM", momentum,
                "NUM_HIDDEN_LAYERS", num_hidden_layers,
                "PERCENT_OF_TRAINING_SET", percent_training_set,
                "NUM_HIDDEN_UNITS_PER_LAYER", num_hidden_units_per_layer)

    nueral_network = create_network(num_inputs, num_output_units, num_hidden_layers, num_hidden_units_per_layer)

    results = []
    for epoch in range(max_epochs):
        correct_predictions = 0
        incorrect_predictions = 0
        iteration_count = 0
        
        prev_output_unit_updates, prev_hidden_unit_updates  = np.zeros(np.shape(nueral_network[0])),np.zeros(np.shape(nueral_network[1]))
        training_subset = select_random_subset(training_images, training_labels, retain_percent=percent_training_set);
        training_subset_size = len(training_subset)
        for training_image, training_label in training_subset: 
            weights_to_output_unit, weights_to_hidden_units, prev_output_unit_updates, prev_hidden_unit_updates  = \
                run_iteration(nueral_network, training_image, 
                                training_label, prev_output_unit_updates, 
                                prev_hidden_unit_updates, learning_rate, momentum)
            nueral_network  = (weights_to_output_unit, weights_to_hidden_units) 
            predicted_label = make_prediction(nueral_network, training_image)
            if predicted_label != training_label: incorrect_predictions+=1
            else: correct_predictions+=1
            running_accuracy = correct_predictions/(incorrect_predictions+correct_predictions)
            # if (iteration_count % 10000 == 0): print("EPOCH",epoch , "ITERATION_COUNT", iteration_count, "RUNNING_ACCURACY", running_accuracy)
            iteration_count+=1

        # record-test-accuracy 
        epoch_test_accuracy, epoch_test_confusion_matrix  = compute_accuracy(test_images, test_labels, nueral_network)
        epoch_training_accuracy, epoch_training_confusion_matrix  = compute_accuracy(training_images, training_labels, nueral_network)
        
        results.append({ 
            "epoch": epoch, 
            "test_accuracy": epoch_test_accuracy, 
            "test_confusion_matrix":  epoch_test_confusion_matrix,
            "training_accuracy": epoch_training_accuracy,
            "training_confusion_matrix":  epoch_training_confusion_matrix, 
            "nueral_network": nueral_network,
            "training_subset_size":training_subset_size,
        })
        print("EPOCH:", epoch, "TEST_ACCURACY:", epoch_test_accuracy, "TRAINING_ACCURACY", epoch_training_accuracy)
    return results 

def compute_accuracy(images, labels, nueral_network):
   """
   Compute the labeling accuracy of the nueral network
   """
   incorrect = 0 
   correct = 0 
   confusion_matrix = np.zeros((NUM_OUTPUT_UNITS, NUM_OUTPUT_UNITS))
   for image, label in zip(images, labels):
        predicted_label = make_prediction(nueral_network, image)
        confusion_matrix[predicted_label][label] += 1
        if label == predicted_label: correct+=1
        else: incorrect+=1
   accuracy = correct/(correct+incorrect)
   return (accuracy, confusion_matrix)

def make_prediction(nueral_network, image):
    """
       Make prediction on an input image returns the index of the 
       output unit the forward pass which best classifies it as being in class. 
    """ 
    scaled_training_image = scale_image(image)
    activations = run_forward_phase(scaled_training_image, nueral_network) 
    return np.argmax(select_output_activations(activations))

def run_iteration(nueral_network, input_image, 
                    training_label, prev_output_unit_updates, 
                    prev_hidden_unit_updates, learning_rate, momentum):
    """Run one iteration of nueral network."""
    scaled_training_image = scale_image(input_image)

    forward_phase_activations = run_forward_phase(scaled_training_image, nueral_network) 

    weights_to_output_unit, weights_to_hidden_units, \
        output_unit_updates, hidden_unit_updates = \
            run_backward_phase(nueral_network, 
                                   forward_phase_activations,
                                   scaled_training_image,
                                   training_label,
                                   prev_output_unit_updates, 
                                   prev_hidden_unit_updates,
                                   learning_rate,
                                   momentum)

    return (weights_to_output_unit, weights_to_hidden_units, output_unit_updates, hidden_unit_updates)

def scale_image(input_image):
    """
        Scale the weights of images by deviding by the maximum 
        pixel intenisity which any point can have.
    """
    return np.multiply(1/255.0,  input_image)

default_config = { 
    "learning_rate": 0.1, 
    "momentum":0.9, 
    "num_hidden_units_per_layer": 100, 
    "percent_training_set": 1 
}

"""
    Contains the list of experments which we are supposed to run by name.
"""
experiments = [
    { "name":"1-a","learning_rate": 0.1, "momentum":0.9, "num_hidden_units_per_layer": 20, "percent_training_set": 1},
    { "name":"1-b","learning_rate": 0.1, "momentum":0.9, "num_hidden_units_per_layer": 50, "percent_training_set": 1},
    { "name":"1-c","learning_rate": 0.1, "momentum":0.9, "num_hidden_units_per_layer": 100, "percent_training_set": 1},
    { "name":"2-a","learning_rate": 0.1, "momentum":0.0, "num_hidden_units_per_layer": 100, "percent_training_set": 1},
    { "name":"2-b","learning_rate": 0.1, "momentum":0.25, "num_hidden_units_per_layer": 100, "percent_training_set": 1},
    { "name":"2-c","learning_rate": 0.1, "momentum":0.5,  "num_hidden_units_per_layer" : 100, "percent_training_set": 1},
    { "name":"3-a","learning_rate": 0.1, "momentum":0.9,  "num_hidden_units_per_layer" : 100, "percent_training_set": 0.25},
    { "name":"3-b","learning_rate": 0.1, "momentum":0.9,  "num_hidden_units_per_layer" : 100, "percent_training_set": 0.50},
    { "name":"small-model","learning_rate": 0.1, "momentum":0.9,  "num_hidden_units_per_layer" : 100, "percent_training_set": 0.15},
]

def select_training_accuracy(experiment): 
    return 100.0 * experiment['training_accuracy']

def select_test_accuracy(experiment):     
    return 100.0 * experiment['test_accuracy']

def select_training_accuracies(experiment_result):
    return [select_training_accuracy(experiment) for experiment in experiment_result] 

def select_test_accuracies(experiment_result):
    return [select_test_accuracy(experiment) for experiment in experiment_result] 
    
def select_epoch(experiment): return experiment['epoch']

def select_name(experiment): return experiment['name']

def plot_experiment_accuracy(experiment_arguments, experiment_result, axis):
    """
    Populates the axis with a plot of training and test accuracies. 
    """
    sorted_experiments  = sorted(experiment_result, key=lambda experiment: experiment['epoch'])
    max_epoch = sorted_experiments[-1]['epoch']
    training_accuracies = select_training_accuracies(sorted_experiments) 
    test_accuracies     = select_test_accuracies(sorted_experiments) 

    common_plot_config   = { 'linestyle':'dashed', 'linewidth':0.5, 'markersize':2 }
    training_plot_config = { 'color':'green', 'marker':'o', 'label':'Training Accuracy'}
    test_plot_config     = { 'color':'red',   'marker':'x', 'label':'Test Accuracy' }

    axis.plot(range(max_epoch+1), training_accuracies, **{**training_plot_config, **common_plot_config})
    axis.plot(range(max_epoch+1), test_accuracies, **{**test_plot_config,**common_plot_config})

    axis.set_title('Learning Rate: %.2f, Momentum:%.2f, Hidden Units: %d, Training Set Usage: %.2f' % 
            (experiment_arguments["learning_rate"], 
             experiment_arguments["momentum"], 
             experiment_arguments["num_hidden_units_per_layer"], 
             experiment_arguments["percent_training_set"]*100) , pad=10)
    axis.set_xlabel('Epoch') 
    axis.set_yticks(np.arange(90, 100, .5))
    axis.set_ylabel('Accuracy')
    axis.legend()
    return axis

def plot_experiment_confusion_matrix(experiment_arguments, experiment, axis):
    matrix = experiment['test_confusion_matrix']
    sns.heatmap(matrix, annot=True, fmt='g', cmap="YlGnBu", linewidths=.5, ax=axis)
    axis.set_title('Confusion Matrix after '+str(experiment['epoch']) + ' Epoch(s)' , pad=25, loc="left")
    axis.margins(.15)
    return axis

def save_experiment_plot(experiment_arguments, experiment_result, save_folder, name="run"):
    sorted_experiments  = sorted(experiment_result, key=select_epoch)
    figure, (accuracy_ax, confidence_ax) = plt.subplots(nrows=2, ncols=1)
    figure.set_figheight(14)
    figure.set_figwidth(15)

    # Plot the accuracy of the experment over epochs
    plot_experiment_accuracy(experiment_arguments, experiment_result, accuracy_ax)

    # Add the confusion matrix
    plot_experiment_confusion_matrix(experiment_arguments,sorted_experiments[-1], confidence_ax)
    
    plot_name = name+'-plot.png'
    plot_path = os.path.join(save_folder, plot_name)
    plt.savefig(plot_path)
    plt.cla()
    return plot_path

class NumpyEncoder(json.JSONEncoder):
    """ Special json encoder for numpy types """
    def default(self, obj):
        if isinstance(obj, np.integer):
            return int(obj)
        elif isinstance(obj, np.floating):
            return float(obj)
        elif isinstance(obj, np.ndarray):
            return obj.tolist()
        return json.JSONEncoder.default(self, obj)

def save_experiment_json(experiment_arguments, experiment_result, save_folder, name="run"):
    """
    Saves the result of a run into a json file for future 
    reference.
    """
    file_name = name+"-experiment-result.json"
    file_path = os.path.join(save_folder, file_name)
    result_file = open(file_path, 'w') 
    json.dump(experiment_result, result_file, cls=NumpyEncoder)
    result_file.close()

    file_name = name+"-experiment-config.json"
    file_path = os.path.join(save_folder, file_name)
    config_file = open(file_path, 'w') 
    json.dump(experiment_arguments, config_file, cls=NumpyEncoder)
    config_file.close()

    return file_path

def save_experiment_result(experiment_arguments, experiment_result, save_directory="../runs", name="run"):
    """
        Create a plot fo the experimental result and its confusion matrix, 
        for future reference.  Also saves the run result in the 
        save_directory as a timestamped run.
    """
    save_folder = create_experimental_result_directory(name=name, save_directory=save_directory) 
    if not save_folder: raise RuntimeError('Could not create folder in save directory %s ' % save_directory)
    save_experiment_plot(experiment_arguments, experiment_result, save_folder, name = name)
    save_experiment_json(experiment_arguments, experiment_result, save_folder, name=name) 
    return save_folder

def create_experimental_result_directory(name="run", save_directory="../runs"):
    """
    Creates a runs folder with results of an experimental directory run.
    """
    folder_time = datetime.now().strftime("%Y-%m-%d_%I-%M-%S_%p") 
    folder_name =  name + "_" + folder_time
    save_folder =os.path.join(save_directory, folder_name)
    os.makedirs(save_folder,0o777)
    return save_folder

def run_experiment(experiment_arguments):
    """
    Runs trainging with an experimental configuration.
    """
    result = train(**{**{'max_epochs':50, **experiment_arguments}})
    save_experiment_result(experiment_arguments, result, name=select_name(experiment_arguments))
    return result

if __name__ == '__main__':
    """
    Run experiments by name, for example:
        ./visualize-run.py --path "1-c"
    """
    parser = argparse.ArgumentParser(description='Conduct exeriment on MNIST dataset')
    parser.add_argument('--runpath', help='Read in the file which represents the run.')
    parser.add_argument('--experiment', help='The experiment represented by the run path.')
    args = parser.parse_args()
    config_path =  os.path.join(args.runpath,str(args.experiment)+'-experiment-config.json')
    result_path = os.path.join(args.runpath,str(args.experiment)+'-experiment-result.json')

    result_file = open(result_path) 
    config_file = open(config_path) 

    experiment_result = json.load(result_file)
    experiment_arguments  = json.load(config_file)
    save_experiment_result(experiment_arguments, experiment_result, name=experiment_arguments['name']+'-regenerated')
