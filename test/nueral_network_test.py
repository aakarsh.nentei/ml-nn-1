from mnist import MNIST
import pytest
from src.main import  create_network, run_backward_phase, run_forward_phase, run_iteration, scale_image, sigmoid 
import numpy as np

def test_default():
    print("first test run ");
    assert True == True

def create_nn_fixture():
    num_hidden_layers=1
    num_output_units=10
    num_inputs = 784
    num_hidden_units_per_layer=20
    (output_units, hidden_units) = create_network(num_inputs, 
                                        num_output_units, 
                                        num_hidden_layers, 
                                        num_hidden_units_per_layer)
    return (output_units, hidden_units)
 
def test_create_network():
    (output_units, hidden_units) = create_nn_fixture()
    assert (21,10) == np.shape(output_units)
    assert (785,20) == np.shape(hidden_units)

    # Assert that random numbers are honoring weight ranges.
    for i in range(21):
        for j in range(10):
            assert output_units[i][j] <= 0.5 and output_units[i][j] >= -0.5

    for i in range(785):
        for j in range(20):
            assert hidden_units[i][j] <= 0.5 and hidden_units[i][j] >= -0.5

def test_vectorized_sigmoid():
    """Assert that sigmoid approches limits on large values."""
    assert np.isclose(0.5, sigmoid([0])[0])
    assert np.isclose(1.,  sigmoid([100])[0])
    assert np.isclose(0.,  sigmoid([-100])[0])

def test_forward_phase():
   nn = create_nn_fixture() 
   mndata = MNIST('../data')
   training_images, training_labels = mndata.load_training()
   training_image = training_images[0]
   scaled_input_image_with_bias = scale_image(training_image)
   output = run_forward_phase(scaled_input_image_with_bias, nn)

def test_backward_phase():
   nueral_network = create_nn_fixture() 
   mndata = MNIST('../data')
   training_images, training_labels = mndata.load_training()
   training_image = training_images[0]
   training_label = training_labels[0]

   scaled_input_image_with_bias = scale_image(training_image)

   output_activations, hidden_unit_activations_with_bias = run_forward_phase(scaled_input_image_with_bias, nueral_network)

   # backward phase 
   run_backward_phase(nueral_network, 
                           output_activations, 
                           hidden_unit_activations_with_bias, 
                           scaled_input_image_with_bias,
                           training_label)


def test_run_iteration():
   nueral_network = create_nn_fixture() 
   mndata = MNIST('../data')
   training_images, training_labels = mndata.load_training()
   training_image = training_images[0]
   training_label = training_labels[0]
   run_iteration(nueral_network, training_image, training_label)

