from mnist import MNIST
from src.main import  create_network 
from src.main import save_experiment_result
import random
import numpy as np

def create_nn_fixture():
    num_hidden_layers=1
    num_output_units=10
    num_inputs = 784
    num_hidden_units_per_layer=20
    (output_units, hidden_units) = create_network(num_inputs, num_output_units, num_hidden_layers, 
                                        num_hidden_units_per_layer)
    return (output_units, hidden_units)


def random_confusion_matrix(size, iterations):
    matrix = np.zeros((size, size)) 
    for i in range(iterations):
        predicted_integer =  random.randint(0, size-1) 
        actual_integer =  random.randint(0, size-1) 
        matrix[predicted_integer][actual_integer]+=1
    return matrix

def random_experimental_result():
    test_experimental_result = []
    for i in range(50):
        test_experimental_result.append({
            'name': 'unit-test-experiment-1' , 
             'epoch': i, 
             'test_accuracy': 1-random.uniform(0,1.0), 
             'test_confusion_matrix':  random_confusion_matrix(10, 100),
             'training_accuracy': 1-random.uniform(0,1.0),
             'training_confusion_matrix':  random_confusion_matrix(10, 100)
        })
    return test_experimental_result
 
def test_save_experimental_result():
    test_experimental_result = random_experimental_result()
    test_experimental_arguments = { 
        "name":"2-c",
        "learning_rate": 0.1, 
        "momentum":0.5,  
        "num_hidden_units_per_layer" : 100, 
        "percent_training_set": 1
    }

    save_experiment_result(test_experimental_arguments, test_experimental_result, name="unit-test-save-experimental-result")


